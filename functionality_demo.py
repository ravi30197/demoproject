import time
from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver import ActionChains, Keys
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait


serv_obj = Service("/Users/dit46/Browserdrivers/chromedriver-mac-arm64/chromedriver")
driver = webdriver.Chrome(service=serv_obj)
driver.maximize_window()
driver.delete_all_cookies()
driver.implicitly_wait(30)
start_time = time.time()
driver.get("https://vogue-fashion-demo.mybigcommerce.com/")

# WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//label[normalize-space()='Hassle Free Returns']")))

end_time = time.time()
page_load_time = end_time - start_time
print(f"Page loaded in {page_load_time:.2f}")

click_account_button = driver.find_element(By.XPATH, "//a[@href='/account.php']").click()
# time.sleep(2)
click_signin = driver.find_element(By.XPATH, "//div[@class='headerbox']//li[1]//a[1]").click()
# time.sleep(5)
email = driver.find_element(By.ID, "login_email").send_keys("jamescolib56789@gmail.com")
password = driver.find_element(By.ID, "login_pass").send_keys("testing@123")

signin = driver.find_element(By.XPATH, "//input[@value='Sign in']").click()

# try:
#     error_message_element = driver.find_element(By.XPATH,"//div[@class='form-group'][contains(@class, 'alert-danger')]")
#     assert "Invalid email or password" in error_message_element.text, f"Expected error message not found. Actual error message: {error_message_element.text}"
# except NoSuchElementException:
#     print("Your email address or password is incorrect. Please try again. If you've forgotten your sign in details, just click the 'Forgot your password?' link below")
# except AssertionError as e:
#     print(e)
time.sleep(5)

#other login details
# newemail=driver.find_element(By.ID, "login_email").send_keys("jamescolib56789@gmail.com")
# newpass=driver.find_element(By.ID, "login_pass").send_keys("testing@123")
# newsignin=driver.find_element(By.XPATH, "//input[@value='Sign in']").click()
# time.sleep(5)

#click logo for move to home page
click_logo=driver.find_element(By.CLASS_NAME,"header-logo.header-logo--center.logo-image").click()
time.sleep(4)

driver.execute_script("window.scrollBy(0, 500);")

#hover product card on home page
home_card_hover=driver.find_element(By.XPATH,"//div[@class='featured-products productCarousel-section']//li[6]")
actions=ActionChains(driver)
actions.move_to_element(home_card_hover).perform()

#cardquickview button process
click_quickview_button=driver.find_element(By.XPATH,"//body[1]/main[1]/div[2]/div[1]/div[2]/ul[1]/div[1]/div[1]/li[6]/article[1]/figure[1]/figcaption[1]/div[1]/button[1]").click()

#quickview in variant select
quick_view_color=driver.find_element(By.XPATH,"//label[@for='attribute_swatch_202_320']").click()
quick_view_size=driver.find_element(By.CLASS_NAME,"form-select.form-select--small")
select= Select(quick_view_size)
select.select_by_visible_text("XXL")
addtocart=driver.find_element(By.ID,"form-action-addToCart").click()
time.sleep(5)
popup_cancel_icon=driver.find_element(By.XPATH,"//div[@id='previewModal']//button[@title='Close']").click()

#Most popular product select
dotselect=driver.find_element(By.XPATH,"//div[@class='owl-item active']//div//span[@class='dot dot-three']").click()
time.sleep(5)

driver.execute_script("window.scrollTo(0,0);")

#search product
searchbox=driver.find_element(By.XPATH,"//input[@id='search-query']")
actions1=ActionChains(driver)
actions1.send_keys_to_element(searchbox,"men")
actions1.send_keys(Keys.ENTER)
actions1.perform()

#search page action
pricemin=driver.find_element(By.NAME,"min_price").send_keys(20)
pricemax=driver.find_element(By.NAME,"max_price").send_keys(1000)
update=driver.find_element(By.XPATH,"//button[normalize-space()='Update']").click()

time.sleep(5)

hover_men=driver.find_element(By.XPATH,"//a[@class='navPages-action menusimple has-subMenu'][normalize-space()='Men']")
actions2=ActionChains(driver)
actions2.move_to_element(hover_men).perform()
subcategory_select=driver.find_element(By.XPATH,"//a[normalize-space()='Shirts']").click()

#category page in action
category_check_box=driver.find_element(By.XPATH,"//div[@id='facetedSearch-content--brand']//li[1]//a[1]").click()
time.sleep(3)
driver.execute_script("window.scrollBy(0, 500);")
categoryPDP=driver.find_element(By.XPATH,"//a[contains(@href,'https://vogue-fashion-demo.mybigcommerce.com/aloha-maxi-coat/')]//div[contains(@class,'card-img-container')]")
categoryPDP.click()

#PDP page action (currently add to cart functionality add we can add other functionality according to requirement )
# pdp_color_variant=driver.find_element(By.XPATH,"//div[@class='form-option-wrapper']").click()
# size_dropdown_click=driver.find_element(By.CLASS_NAME,"form-select.form-select--small").click()
# select1=Select(size_dropdown_click)
# select1.select_by_visible_text("XL")
wishlist_icon_click=driver.find_element(By.XPATH,"//form[@class='form form-wishlist form-action']").click()
click_add_to_wishlist=driver.find_element(By.XPATH,"//form[@class='form form-wishlist form-action']//li[1]").click()

#return to product page from whislist page
product_card_click=driver.find_element(By.XPATH,"//a[@href='https://vogue-fashion-demo.mybigcommerce.com/aloha-maxi-coat/']//div[@class='card-img-container']").click()
product_page_addtocart=driver.find_element(By.XPATH,"//button[@id='form-action-addToCart']").click()
go_to_cartpage=driver.find_element(By.XPATH,"//a[normalize-space()='View or edit your cart']").click()
time.sleep(5)
cancel_product=driver.find_element(By.XPATH,"//button[@aria-label='Remove Aloha Maxi Coat from cart']").click()

confirm_cancel_pdp=driver.find_element(By.CLASS_NAME,"confirm.button").click()
time.sleep(5)
checkout=driver.find_element(By.XPATH,"//a[normalize-space()='Check out']").click()
time.sleep(8)

#checkout page in shipping method select(according requirement we can change it)
shipping__method=driver.find_element(By.CLASS_NAME,"form-checklist-header.form-checklist-header--selected").click()
continue_button=driver.find_element(By.CLASS_NAME,"button.button--primary.optimizedCheckout-buttonPrimary").click()
time.sleep(10)
driver.execute_script("window.scrollBy(0, 2000);")
#payment method select and process
codselect=driver.find_element(By.CLASS_NAME,"form-checklist-header.form-checklist-header--selected").click()
place_order=driver.find_element(By.XPATH,"//button[@id='checkout-payment-continue']").click()
time.sleep(5)


driver.quit()


