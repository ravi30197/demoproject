import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from faker import Faker

screen_sizes = [(1024,800),(1280, 800)]
                # (1440,1024),(1600,1024),(1680,1024),(1920, 1080)]  # Add more sizes as needed
# Initialize the WebDriver (change the driver path as needed)
serv_obj = Service("/Users/dit46/Browserdrivers/chromedriver-mac-arm64/chromedriver")
driver = webdriver.Chrome(service=serv_obj)

for width, height in screen_sizes:
    # Set the browser's viewport size
    driver.set_window_size(width, height)
    # driver.set_window_size(1550,1024)
    driver.implicitly_wait(30)

    # Record start time
    start_time = time.time()

    driver.get("https://vogue-fashion-demo.mybigcommerce.com/")

    # Explicitly wait for some element to indicate the page has fully loaded (adjust as needed)
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, "//label[normalize-space()='Hassle Free Returns']")))

    # Record end time
    end_time = time.time()

    # Calculate and print page load time
    page_load_time = end_time - start_time
    print(f"Page loaded in {page_load_time:.2f} seconds for screen size {width}x{height}")

# Quit the driver after all iterations
driver.quit()

